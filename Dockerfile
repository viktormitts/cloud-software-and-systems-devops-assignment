FROM python:latest
WORKDIR /application
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY . .
EXPOSE 8000
CMD uvicorn main:app --reload
